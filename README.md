# Assignment 6 - Create a database and access it

## Appendix A: SQL script to create database
Made several scripts for creating and altering a database, creating tables and inserting, updating and deleting data.
The scrips will first make a database named SuperheroesDB. Then its associated tabels, Superhero-table, an Assisten-table, a Power-table and a joining-table called SuperheroPower. Then finally adding, updating and removing data.

### Folders
#### DatabaseFiles
This folder contains all the scripts.


## Appendix B: Reading data with SQL Client
Used Visual Studio to manipulate SQL Server data through the console using the SQL Client library. The database is created by using the Chinook-script. The Program-file shows all the test-methods used for testing the methods  manipulating the data and the print-methods for the outputs.

### Folders
#### Models
This folder contains the data structure for each class(model).

#### Repositories
This folder contains three files. 
* ConnectionHelper connects the project to the database.
* CustomerRepository contains all the methods that interacts with the database.
* ICustomerRepository is an interface with all the methods used in the Program-file. 

