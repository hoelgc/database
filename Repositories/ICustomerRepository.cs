﻿using Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Repositories
{
    public interface ICustomerRepository
    {
        public Customer GetCustomer(int id);
        public Customer GetCustomerByLastName(string name);
        public List<Customer> GetAllCustomers();
        public List<Customer> GetCustomerPage(int offset, int limit);
        public bool AddNewCustomer(Customer customer);
        public bool UpdateCustomer(Customer customer);
        public List<CustomerCountry> CountByCountry();
        public List<CustomerSpender> GetHighestSpenders();
        public List<CustomerGenre> GetTopGenre(int id);
    }
}
