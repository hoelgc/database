﻿using Database.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// A method which retrieves a specific customer by ID from the database with a SELECT query.
        /// A connection to the database is being created by calling the GetConnectionstring method in the ConnectionHelper class.
        /// After opening the connection a SqlCommand is being created. This takes inn the SQL query variable (for the DB
        /// to know what to query) and the opened connection to the DB.
        /// To read the data which is found, the SqlDataReader is used and a while loop loops through all the results for each specified column.
        /// Here a CustomerGenre object is created and the results from the reader will be added to this object.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>One customer</returns>
        public Customer GetCustomer(int id)
        {
            Customer customer = new Customer();
            string query = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
                " WHERE CustomerId = @CustomerId";

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(query, connection))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.GetString(4);
                                customer.Phone = reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Could not find customer. Exited with message: " + ex.Message);
            }
            return customer;

        }

        /// <summary>
        /// A method which retrieves a specific customer by checking on it's surname from the database with a SELECT query.
        /// A connection to the database is being created by calling the GetConnectionstring method in the ConnectionHelper class.
        /// After opening the connection a SqlCommand is being created. This takes inn the SQL query variable (for the DB
        /// to know what to query) and the opened connection to the DB.
        /// To read the data which is found, the SqlDataReader is used and a while loop loops through all the results for each specified column.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>One customer</returns>
        public Customer GetCustomerByLastName(string name)
        {
            Customer customer = new Customer();
            string query = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
                " WHERE LastName LIKE @LastName";

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(query, connection))
                    {
                        cmd.Parameters.AddWithValue("@LastName", "%"+name+"%");
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.GetString(4);
                                customer.Phone = reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Could not find customer. Exited with message: " + ex.Message);
            }
            return customer;

        }

        /// <summary>
        /// A method which gets all the customers from the database with a SELECT query.
        /// A connection to the database is being created by calling the GetConnectionstring method in the ConnectionHelper class.
        /// After opening the connection a SqlCommand is being created. This takes inn the SQL query variable (for the DB
        /// to know what to query) and the opened connection to the DB.
        /// To read the data which is found, the SqlDataReader is used and a while loop loops through all the results for each specified column.
        /// For every row in the DB it creates a new customer and collects the specified details about that customer.
        /// The loop ends with the customer being added to a list of customers.
        /// </summary>
        /// <returns>A list of Customers</returns>
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customerList = new List<Customer>();
            string query = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(query, connection))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer tempCustomer = new Customer();
                                 
                                tempCustomer.CustomerId = reader.GetInt32(0);
                                tempCustomer.FirstName = reader.GetString(1);
                                tempCustomer.LastName = reader.GetString(2);
                                tempCustomer.Country = reader.IsDBNull(3) ? "NO COUNTRY" : reader.GetString(3);
                                tempCustomer.PostalCode = reader.IsDBNull(4) ? "NO POSTAL" : reader.GetString(4);
                                tempCustomer.Phone = reader.IsDBNull(5) ? "NO PHONE" : reader.GetString(5);
                                tempCustomer.Email = reader.GetString(6);
                                customerList.Add(tempCustomer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("No customers found. Exited with message: " + ex.Message);
            }
            return customerList;
        }

        /// <summary>
        /// Takes in a customer object where the query is stating that "this" customer should be inserted into the Customer table.
        /// A connection to the database is being created by calling the GetConnectionstring method in the ConnectionHelper class.
        /// After opening the connection a SqlCommand is being created. This takes inn the SQL query variable (for the DB
        /// to know what to query) and the opened connection to the DB.
        /// To read the data which is found, the SqlDataReader is used and a while loop loops through all the results for each specified column.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>True if the customer is successfully created and false if it's not.</returns>
        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string query = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Could not add customer. Exited with message: " + ex.Message);
            }
            return success;
        }

        /// <summary>
        /// Takes in a customer object where the query is making sure that the changes which is done is affecting the correct customer by using the 
        /// "WHERE CustomerId = @CustomerId"-statement. Even though the method takes in a whole customer object the 
        /// columns affected will only be the ones who is actually changed, the remaining fields will remain unchanged.
        /// A connection to the database is being created by calling the GetConnectionstring method in the ConnectionHelper class.
        /// After opening the connection a SqlCommand is being created. This takes inn the SQL query variable (for the DB
        /// to know what to query) and the opened connection to the DB.
        /// To read the data which is found, the SqlDataReader is used and a while loop loops through all the results for each specified column.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>True if the customer is successfully updated and false if it's not.</returns>
        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string query = "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email WHERE CustomerId = @CustomerId ";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Could not update customer. Exited with message: " + ex.Message);
            }
            return success;
        }

        /// <summary>
        /// A list of CustomerCountry objects is created where all instances of different countries will be added to this list.
        /// A connection to the database is being created by calling the GetConnectionstring method in the ConnectionHelper class.
        /// After opening the connection a SqlCommand is being created. This takes inn the SQL query variable (for the DB
        /// to know what to query) and the opened connection to the DB.
        /// To read the data which is found, the SqlDataReader is used and a while loop loops through all the results for each specified column.
        /// Here a CustomerCountry object is created and the results from the reader will be added to this object.
        /// </summary>
        /// <returns>A list with the number of customers based on the country the customer is registered with.</returns>
        public List<CustomerCountry> CountByCountry()
        {
            List<CustomerCountry> countryList = new List<CustomerCountry>();
            string query = "SELECT Country, COUNT(DISTINCT CustomerId) AS Amount " +
                "FROM Customer " +
                "GROUP BY Country " +
                "ORDER BY Amount DESC ";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry tempCountry = new CustomerCountry();

                                tempCountry.Country = reader.GetString(0);
                                tempCountry.Count = reader.GetInt32(1);
                                countryList.Add(tempCountry);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Could not count customers by country. Exited with message: " + ex.Message);
            }
            return countryList;
        }

        /// <summary>
        /// The method takes in an offset and a limit parameter which decides where in the table the data should be retrieved from and how many objects
        /// which shall be retrieved.
        /// A connection to the database is being created by calling the GetConnectionstring method in the ConnectionHelper class.
        /// After opening the connection a SqlCommand is being created. This takes inn the SQL query variable (for the DB
        /// to know what to query) and the opened connection to the DB.
        /// To read the data which is found, the SqlDataReader is used and a while loop loops through all the results for each specified column.
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns>A list of customers based on parameters</returns>
        public List<Customer> GetCustomerPage(int offset, int limit)
         {
            List<Customer> page = new List<Customer>();
            string query = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer " +
                "ORDER BY CustomerId DESC " +
                "OFFSET @offset ROWS " +
                "FETCH NEXT @limit ROWS ONLY";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.AddWithValue("@offset", offset);
                        cmd.Parameters.AddWithValue("@limit", limit);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer tempCustomer = new Customer();

                                tempCustomer.CustomerId = reader.GetInt32(0);
                                tempCustomer.FirstName = reader.GetString(1);
                                tempCustomer.LastName = reader.GetString(2);
                                tempCustomer.Country = reader.IsDBNull(3) ? "NO COUNTRY" : reader.GetString(3);
                                tempCustomer.PostalCode = reader.IsDBNull(4) ? "NO POSTAL" : reader.GetString(4);
                                tempCustomer.Phone = reader.IsDBNull(5) ? "NO PHONE" : reader.GetString(5);
                                tempCustomer.Email = reader.GetString(6);
                                page.Add(tempCustomer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Could not retrieve requested list of customers. Exited with message: " + ex.Message);
            }
            return page;
        }

        /// <summary>
        /// Firstly a list of all customers is being created. The SQL query specifies that the customerId, name and total amount of money the customers 
        /// has been spending on tracks shall be added to the list in descending order.
        /// Then a connection to the database is being created by calling the GetConnectionstring method in the ConnectionHelper class.
        /// After opening the connection a SqlCommand is being created. This takes inn the SQL query variable (for the DB
        /// to know what to query) and the opened connection to the DB.
        /// To read the data which is found, the SqlDataReader is used and a while loop loops through all the results for each specified column.
        /// </summary>
        /// <returns> A list of customers sorted by how much money they have been using on tracks </returns>
        public List<CustomerSpender> GetHighestSpenders()
        {
            List<CustomerSpender> spenders = new List<CustomerSpender>();
            string query = "SELECT Customer.CustomerId, Customer.FirstName, Customer.LastName, SUM (Total) AS TotalSum " +
                "FROM Customer " +
                "INNER JOIN Invoice " +
                "ON Customer.CustomerId = Invoice.CustomerId " +
                "GROUP BY Customer.CustomerId, Customer.FirstName, Customer.LastName " +
                "ORDER BY TotalSum DESC ";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {        
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender customerSpender = new CustomerSpender();

                                customerSpender.CustomerId = reader.GetInt32(0);
                                customerSpender.FirstName = reader.GetString(1);
                                customerSpender.LastName = reader.GetString(2);
                                customerSpender.TotalSum = reader.GetDecimal(3);
                                spenders.Add(customerSpender);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Could not retrieve a list of customers ordered by the highest spender. Exited with message: " + ex.Message);
            }
            return spenders;
        }

        /// <summary>
        /// A list of CustomerGenre objects is created where all instances of different genres (if same track count) will be added to this list.
        /// To be able to list out details from the customer, invoice, track and genre tables, INNER JOIN was used to "connect" the tables.
        /// A connection to the database is being created by calling the GetConnectionstring method in the ConnectionHelper class.
        /// After opening the connection a SqlCommand is being created. This takes inn the SQL query variable (for the DB
        /// to know what to query) and the opened connection to the DB.
        /// To read the data which is found, the SqlDataReader is used and a while loop loops through all the results for each specified column.
        /// Here a CustomerGenre object is created and the results from the reader will be added to this object.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The genre(s) that the customer with a specific CustomerId listens to the most
        /// If multiple genres with the same track count, all applicable genres will be added to the list.</returns>
        public List<CustomerGenre> GetTopGenre(int id)
        {
            List<CustomerGenre> topGenres = new List<CustomerGenre>();
            string query = "SELECT TOP 1 WITH TIES Customer.CustomerId, Customer.FirstName, Customer.LastName, Genre.Name AS Genre, COUNT ( Genre.Name ) AS GenreCount " +
            "FROM Customer " +
            "INNER JOIN Invoice " +
            "ON Customer.CustomerId = Invoice.CustomerId " +
            "INNER JOIN InvoiceLine " +
            "ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
            "INNER JOIN Track " +
            "ON InvoiceLine.TrackId = Track.TrackId "+
            "INNER JOIN Genre " +
            "ON Track.GenreId = Genre.GenreId " +
            "WHERE Customer.CustomerId = '12' " +
            "GROUP BY Customer.FirstName, Customer.LastName, Genre.Name, Customer.CustomerId " +
            "ORDER BY GenreCount DESC ";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre customerGenre = new CustomerGenre();

                                customerGenre.CustomerId = reader.GetInt32(0);
                                customerGenre.FirstName = reader.GetString(1);
                                customerGenre.LastName = reader.GetString(2);
                                customerGenre.Genre = reader.GetString(3);
                                customerGenre.GenreCount = reader.GetInt32(4);
                                topGenres.Add(customerGenre);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Could not retrieve a list of the most popular genre for the requested customer. Exited with message: " + ex.Message);
            }
            return topGenres;
        }

    }
}
