﻿using Database.Models;
using Database.Repositories;
using System;
using System.Collections.Generic;

namespace Database
{
    class Program
    {
        static void Main(string[] args)
        {
            // Simulating Dependency Injection (highly simplified)
            ICustomerRepository repository = new CustomerRepository();
            
            /*Testing methods: */

            //TestGetAllCustomers(repository);
            //TestGetCustomer(repository);
            //TestGetCustomerByName(repository);
            //TestInsert(repository);
            //TestCountByCountry(repository);
            //TestUpdateCustomer(repository);
            //TestGetCustomerPage(repository);
            //TestGetHighestSpenders(repository);
            TestGetTopGenre(repository);
        }

        #region Get Customers by different criteria - Test methods
        static void TestGetAllCustomers(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAllCustomers());
        }

        static void TestGetCustomer(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomer(5));
        }

        static void TestGetCustomerByName(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomerByLastName("nse"));
        }

        static void TestGetCustomerPage(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetCustomerPage(10, 10));
        }

        static void TestCountByCountry(ICustomerRepository repository)
        {
            PrintCountries(repository.CountByCountry());
        }

        static void TestGetHighestSpenders(ICustomerRepository repository)
        {
            PrintSpenders(repository.GetHighestSpenders());
        }

        static void TestGetTopGenre(ICustomerRepository repository)
        {
            PrintGenres(repository.GetTopGenre(12));
        }
        #endregion

        #region AddNewCustomer - Test method
        static void TestInsert(ICustomerRepository repository)
        {
            Customer customer = new Customer();
            customer.FirstName = "Nicole";
            customer.LastName = "Bendu";
            customer.Country = "Norge";
            customer.Email = "Bend@univjdbvd.com";
            customer.Phone = "982t2933";
            customer.PostalCode = "8828";

            Console.WriteLine(repository.AddNewCustomer(customer));
        }
        #endregion

        #region UpdateCustomer - Test method
        static void TestUpdateCustomer(ICustomerRepository repository)
        {
            Customer customer = repository.GetCustomer(6);
            customer.FirstName = "Nicole";

            Console.WriteLine(repository.UpdateCustomer(customer));
        }
        #endregion

        #region Print Multiple objects - Methods

        static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        static void PrintCountries(IEnumerable<CustomerCountry> countries)
        {
            foreach (CustomerCountry country in countries)
            {
                PrintCountry(country);
            }
        }

        static void PrintSpenders(IEnumerable<CustomerSpender> spenders)
        {
            foreach (CustomerSpender spender in spenders)
            {
                PrintSpender(spender);
            }
        }

        static void PrintGenres(IEnumerable<CustomerGenre> genres)
        {
            foreach (CustomerGenre genre in genres)
            {
                PrintGenre(genre);
            }
        }
        #endregion

        #region Print Single objects - Methods
        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"--- { customer.CustomerId} \t {customer.FirstName} {customer.LastName} \t {customer.Country} \t {customer.PostalCode} \t {customer.Phone} \t{customer.Email} ---");
        }

        static void PrintCountry(CustomerCountry country)
        {
            Console.WriteLine($"--- { country.Country} \t {country.Count} ---");
        }

        static void PrintSpender(CustomerSpender spender)
        {
            Console.WriteLine($"--- { spender.CustomerId} \t {spender.FirstName} \t {spender.LastName} \t {spender.TotalSum} ---");
        }

        static void PrintGenre(CustomerGenre genre)
        {
            Console.WriteLine($"--- { genre.CustomerId} \t {genre.FirstName} \t {genre.LastName} \t {genre.Genre} \t {genre.GenreCount} ---");
        }
        #endregion
    }
}
