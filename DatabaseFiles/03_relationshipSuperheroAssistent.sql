ALTER TABLE Assistant
ADD SuperheroID int;

ALTER TABLE Assistant
ADD CONSTRAINT FK_Assistant_Superhero
FOREIGN KEY (SuperheroID) REFERENCES Superhero (SuperheroID);