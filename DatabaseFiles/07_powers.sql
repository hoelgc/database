INSERT INTO Power (Name, Description)
VALUES ('Ocular powers', 'The ability to utilize special techniques through the eyes');

INSERT INTO Power (Name, Description)
VALUES ('Superhuman strength', 'The power to exert force and lift weights beyond what is physically possible for an ordinary human being');

INSERT INTO Power (Name, Description)
VALUES ('Flight', 'The power to fly without any outside influence');

INSERT INTO Power (Name, Description)
VALUES ('Accelerated healing factor', 'The power to heal much faster than normally possible');

INSERT INTO SuperheroPowers (SuperheroID, PowerID)
VALUES (1, 1);

INSERT INTO SuperheroPowers (SuperheroID, PowerID)
VALUES (2, 2);

INSERT INTO SuperheroPowers (SuperheroID, PowerID)
VALUES (1, 3);

INSERT INTO SuperheroPowers (SuperheroID, PowerID)
VALUES (2, 3);

