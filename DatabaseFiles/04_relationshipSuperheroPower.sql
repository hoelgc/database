create table SuperheroPowers
(
  SuperheroID int,
  PowerID int,
  CONSTRAINT PK_Superhero_Power PRIMARY KEY (SuperheroID, PowerID),
  CONSTRAINT FK_Superhero 
      FOREIGN KEY (SuperheroID) REFERENCES Superhero (SuperheroID),
  CONSTRAINT FK_Power 
      FOREIGN KEY (PowerID) REFERENCES Power (PowerID)
);