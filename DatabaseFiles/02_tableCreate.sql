CREATE TABLE Superhero(
	SuperheroID int PRIMARY KEY IDENTITY, 
	Name nvarchar(50), 
	Alias nvarchar(50), 
	Origin nvarchar(50)
);

CREATE TABLE Assistant(
	AssistantID int PRIMARY KEY IDENTITY, 
	Name nvarchar(50), 
);

CREATE TABLE Power(
	PowerID int PRIMARY KEY IDENTITY, 
	Name nvarchar(50), 
	Description nvarchar(max)
);